import React, {useState} from 'react';
import {Button, Card, CardActions, CardContent, CardMedia, IconButton, Typography} from '@material-ui/core';
import {useDispatch, useSelector} from 'react-redux';
import useStyles from './styles';
import {apiURL} from "../../config";
import {Link} from "react-router-dom";
import ArrowForwardIcon from '@material-ui/icons/ArrowForward'
import Modal from "../UI/Modal/Modal";
import DeleteIcon from '@material-ui/icons/Delete';
import {deletePhotoRequest} from "../../store/actions/photoActions";

const PhotoItems = ({creator, image, title, id, photoId}) => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const {user} = useSelector(state => state.users);
    const [selectedTile, setSelectedTile] = useState(false);

    const handleClickOpen = () => {
        setSelectedTile(true);
    };

    const handleClose = () => {
        setSelectedTile(false);
    };


    return (
            <>
            <Card className={classes.card} xs={12}>
                <CardMedia className={classes.media} image={apiURL + '/' + image} title={title}/>
                <Button type="button" onClick={handleClickOpen}>
                    Show
                </Button>
                <Typography className={classes.title} gutterBottom variant="h5" component="h2">{title}</Typography>
                {creator && <CardContent>
                    <Typography variant="body1" color="textSecondary" component="p">
                        Author: {creator?.displayName}
                    </Typography>
                </CardContent>}
                <CardActions className={classes.cardActions}>
                    {creator && <IconButton component={Link} to={'/users/' + creator._id}>
                        <ArrowForwardIcon/>
                    </IconButton>}
                    {(user?._id === id) && (
                        <Button size="small" color="primary" onClick={() => dispatch(deletePhotoRequest(photoId))}>
                            <DeleteIcon fontSize="small" /> Delete
                        </Button>
                    )}
                </CardActions>
            </Card>
            <Modal show={selectedTile} close={handleClose}>
                <img className={classes.image} src={apiURL + '/' + image} alt={image}/>
            </Modal>
        </>
    );
};

export default PhotoItems;