import Layout from "./components/UI/Layout/Layout";
import {Redirect, Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import {useSelector} from "react-redux";
import HomePage from "./containers/HomePage/HomePage";
import NewPhoto from "./containers/NewPhoto/NewPhoto";

const App = () => {
    const user = useSelector(state => state.users.user);

    const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
        return isAllowed ?
            <Route {...props}/> :
            <Redirect to={redirectTo}/>
    };

    return (
        <Layout>
            <Switch>
                <Route path="/register" component={Register}/>
                <Route path="/"  exact component={HomePage}/>
                <Route path="/users/:id" component={HomePage}/>
                <ProtectedRoute
                    path="/newPhoto"
                    component={NewPhoto}
                    isAllowed={user}
                    redirectTo="/register"
                />
                <Route path="/login" component={Login}/>
            </Switch>
        </Layout>
    );
};

export default App;
