import photoSlice from "../slices/photoSlice";

export const {
    fetchPhotosRequest,
    fetchPhotosSuccess,
    fetchPhotosFailure,
    singlePersonRequest,
    singlePersonSuccess,
    singlePersonFailure,
    createPhotoRequest,
    createPhotoSuccess,
    createPhotoFailure,
    deletePhotoRequest,
    deletePhotoSuccess,
    deletePhotoFailure,
    clearError,
} = photoSlice.actions;