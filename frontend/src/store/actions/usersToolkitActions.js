import usersSlice from "../slices/usersSlice";

export const {
    registerUserRequest,
    registerUserSuccess,
    registerUserFailure,
    loginUserRequest,
    loginUserSuccess,
    loginUserFailure,
    facebookRequest,
    googleRequest,
    logoutUser,
    clearError,
} = usersSlice.actions;