import {createSlice} from '@reduxjs/toolkit';

const name = 'photos';


const initialState = {
    fetchLoading: false,
    singlePersonLoading: false,
    fetchError: null,
    singlePersonError: null,
    createLoading: false,
    createError: null,
    deleteLoading: false,
    deleteError: null,
    photos: [],
    groupPhotos : [],
};


const photoSlice = createSlice({
    name,
    initialState,
    reducers: {
        fetchPhotosRequest(state, action){
            state.fetchLoading = true;
        },
        fetchPhotosSuccess(state, {payload: photos}){
            state.fetchLoading = false;
            state.photos = photos;
        },
        fetchPhotosFailure(state, {payload: error}){
            state.fetchLoading = false;
            state.fetchError = error;
        },
        singlePersonRequest(state, action){
            state.singlePersonLoading = true;
        },
        singlePersonSuccess(state, {payload: photos}){
            state.singlePersonLoading = false;
            state.groupPhotos = photos;
        },
        singlePersonFailure(state, {payload: error}){
            state.singlePersonLoading = false;
            state.singlePersonError = error;
        },
        createPhotoRequest(state, action){
            state.createLoading = true;
        },
        createPhotoSuccess(state, {payload: photo}){
            state.createLoading = false;
            state.groupPhotos = [...state.groupPhotos, photo]
        },
        createPhotoFailure(state, {payload: error}){
            state.createLoading = false;
            state.createError = error;
        },
        deletePhotoRequest(state, action){
            state.deleteLoading = true;
        },
        deletePhotoSuccess(state, {payload: id}){
            state.groupPhotos = state.groupPhotos.filter(s => s._id !== id);
            state.deleteLoading = false;
        },
        deletePhotoFailure(state, {payload: error}){
            state.deleteLoading = false;
            state.deleteError = error;
        },
        clearError(state, action){
            state.createError = null;
        }
    }
});

export default photoSlice;