import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "../actions/historyActions";
import {
    createPhotoFailure,
    createPhotoRequest,
    createPhotoSuccess,
    deletePhotoFailure,
    deletePhotoRequest,
    deletePhotoSuccess,
    fetchPhotosFailure,
    fetchPhotosRequest,
    fetchPhotosSuccess,
    singlePersonFailure,
    singlePersonRequest,
    singlePersonSuccess
} from "../actions/photoActions";

export function* fetchPhotos(){
    try{
        const {data} = yield axiosApi.get('/photos');
        yield put(fetchPhotosSuccess(data));
    }catch(e){
        yield put(fetchPhotosFailure(e.response.data))
    }
}

export function* fetchPhotosSingle({payload: id}){
    try{
        const {data} = yield axiosApi.get('/photos/' + id);
        yield put(singlePersonSuccess(data));
    }catch(e){
        yield put(singlePersonFailure(e.response.data))
    }
}

export function* createPhoto({payload}){
    try{
        const {data} = yield axiosApi.post('/photos', payload.photoData);
        yield put(createPhotoSuccess(data));
        yield put(historyPush('/users/' + payload.id));
        toast.success('Great you have created!')
    }catch(e){
        yield put(createPhotoFailure(e.response.data))
    }
}

export function* removePhoto({payload: id}){
    try{
        yield axiosApi.delete('/photos/' + id);
        yield put(deletePhotoSuccess(id));
        toast.success('You have deleted');
    }catch(e){
        yield put(deletePhotoFailure(e.response.data))
    }
}

const photosSaga = [
    takeEvery(fetchPhotosRequest, fetchPhotos),
    takeEvery(singlePersonRequest, fetchPhotosSingle),
    takeEvery(createPhotoRequest, createPhoto),
    takeEvery(deletePhotoRequest, removePhoto),
]

export default photosSaga;
