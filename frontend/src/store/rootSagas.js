import {all} from 'redux-saga/effects';
import usersSaga from "./sagas/usersSagas";
import historySagas from "./sagas/historySagas";
import history from "../history";
import photosSaga from "./sagas/photosSagas";

export function* rootSagas() {
    yield all([
        ...usersSaga,
        ...historySagas(history),
        ...photosSaga,
    ])
}

