import React, {useEffect} from 'react';
import {Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {clearError, createPhotoRequest} from "../../store/actions/photoActions";
import PhotoForm from "../../components/PhotoForm/PhotoForm";

const NewPhoto = () => {
    const dispatch = useDispatch();
    const {user} = useSelector(state => state.users)
    const {createLoading, createError} = useSelector(state => state.photos);

    const onSubmit = photoData => {
        const id = user._id;
        dispatch(createPhotoRequest({photoData, id}));
    };

    useEffect(() => {
        return () => {
            dispatch(clearError());
        };
    }, [dispatch]);

    return (
        <>
            <Typography variant="h4">Create a new photo</Typography>
            <PhotoForm
                onSubmit={onSubmit}
                error={createError}
                loading={createLoading}
            />
        </>
    );
};

export default NewPhoto;