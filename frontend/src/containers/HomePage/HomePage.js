import React, {useEffect} from 'react';
import {Grid, CircularProgress, Button, Typography} from '@material-ui/core';
import {useDispatch, useSelector} from 'react-redux';
import useStyles from './styles';
import PhotoItems from "../../components/Post/PhotoItems";
import {Link} from 'react-router-dom';
import {fetchPhotosRequest, singlePersonRequest} from "../../store/actions/photoActions";

const HomePage = ({match}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const {user} = useSelector(state => state.users);
    const {
        singlePersonLoading,
        singlePersonError,
        photos,
        fetchLoading,
        groupPhotos
    } = useSelector((state) => state.photos);


    useEffect(() => {
        if (match.params.id) {
            dispatch(singlePersonRequest(match.params.id));
        }
        dispatch(fetchPhotosRequest());
    }, [dispatch, match.params])

    return !match.params.id ?
        (
            fetchLoading ? <CircularProgress/> : (
                <Grid className={classes.mainContainer} container alignItems="stretch" spacing={3}>
                    {photos && photos.map((p) => (
                        <Grid key={p._id} item xs={12} sm={6} md={3}>
                            <PhotoItems
                                title={p.title}
                                image={p.image}
                                creator={p.creator}
                                id={p._id}
                            />
                        </Grid>
                    ))}
                </Grid>
            )
        ) : (
            singlePersonLoading ? <CircularProgress/> : (
                <Grid item container>
                    {match.params.id && match.params.id === user?._id && <Button component={Link} to="/newPhoto" mb={3}>Add new</Button>}
                    <Grid  container item className={classes.mainContainer} spacing={3}>
                        {groupPhotos.length < 1 ? <div style={{border: '3px solid red', marginTop: '20px'}}>You do not have any photos. Create and
                            Enjoy</div> : (
                                <Grid item container flexdirection="column">
                                    <Grid item>
                                        <Typography variant='h5'>{groupPhotos.length > 0 && groupPhotos[0].creator.displayName}'s
                                            photos</Typography>
                                    </Grid>
                                    <Grid className={classes.mainContainer} container alignItems="stretch" spacing={3}>
                                        {groupPhotos && groupPhotos.map((p) => (
                                            <Grid key={p._id} item xs={12} sm={6} md={3}>
                                                <PhotoItems
                                                    title={p.title}
                                                    image={p.image}
                                                    id={p.creator._id}
                                                    photoId={p._id}
                                                    error={singlePersonError}
                                                />
                                            </Grid>
                                        ))}
                                    </Grid>
                                </Grid>
                           )}
                    </Grid>
                </Grid>
            )
        )
};

export default HomePage;