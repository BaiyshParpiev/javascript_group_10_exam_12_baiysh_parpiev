const express = require('express');
const Photo = require('../models/Photo');
const auth = require("../middleware/auth");
const multer = require('multer');
const {nanoid} = require('nanoid');
const config = require('../config');
const path = require("path");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});


const upload = multer({storage});

router.get('/', async(req, res) => {
  try{
      const photos = await Photo.find().populate('creator', 'displayName');
      res.send(photos);
  }catch{
      res.status(500).send({global: 'Something went wrong'});
  }
});

router.post('/', auth, upload.single('image'), async(req, res) => {
  try{
    const photo = new Photo({
      title: req.body.title,
      creator: req.user._id,
    });

    if(req.file){
      photo.image = 'uploads/' +  req.file.filename;
    }


    await photo.save();
    res.send(photo)
  }catch(e){
    res.status(403).send(e)
  }
});

router.get('/:id', async(req, res) => {
  try{
    const {id} = req.params;
    const photos = await Photo.find({creator: id}).populate('creator', 'displayName');

    if(!photos){
      return res.status(401).send('In this user has not any photo!')
    }

    res.send(photos);
  }catch(e){
    res.sendStatus(500);
  }
});

router.delete('/:id', auth, async(req, res) => {
  try{
    const idPhoto = req.params.id;
    const photo = await Photo.findById(idPhoto);

    if(!req.user._id.equals(photo.creator)) return res.status(403).send({global: 'It is not your photo!'});

    await Photo.findByIdAndRemove(idPhoto);
    res.send({message: 'You have successful deleted'});
  }catch{
    res.status(500).send({global: 'Something went wrong!'});
  }
});

module.exports = router;
