const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Photo = require("./models/Photo");

const run = async () => {
  await mongoose.connect(config.db.url);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [firstUser, secondUser] = await User.create({
    email: 'admin@gmail.com',
    password: 'admin',
    displayName: 'admin',
    token: nanoid(),
  }, {
    email: 'user@gmail.com',
    password: 'user',
    displayName: 'user',
    token: nanoid(),
  });

  await Photo.create({
    title: 'Cocktail',
    image: 'fixtures/mojito.png',
    creator: firstUser,
  },
    {
    title: 'The wedding',
    image: 'fixtures/wedding.jpg',
    creator: secondUser,
  })



  await mongoose.connection.close();
};

run().catch(console.error);