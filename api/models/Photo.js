const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const PhotoSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
});

PhotoSchema.plugin(idValidator);

const Photo = mongoose.model('Photo', PhotoSchema);
module.exports = Photo;
